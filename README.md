# FinTOC-2019

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.  
https://creativecommons.org/licenses/by-nc-sa/4.0/

FinTOC-2019 Shared Task, Financial Document Title Detection  
http://wp.lancs.ac.uk/cfie/shared-task/

Title: UWB@FinTOC-2019 Shared Task: Financial Document Title Detection  
Authors: Tomáš Hercig, Pavel Král (tigi | pkral @ kiv.zcu.cz)  
http://nlp.kiv.zcu.cz

We share under the above mentioned license our contribution to the shared task.

The repository contains our submissions including the fixed dataset  
(for more information see the our paper - UWB@FinTOC-2019 Shared Task.pdf).

The dataset includes the original and fixed train and test data, however the test data only contain zero labels.
In order to map the labels in the test files we included additional column containing the corresponding original line position.


Citation

Please, cite our article if you use any of the available resources.

@InProceedings{UWB2019fintoc,  
  author    = {Hercig, Tom\'{a}\v{s}  and  Kr'{a}l, Pavel},  
  title     = {{UWB@FinTOC-2019 Shared Task: Financial Document Title Detection}},  
  booktitle = {{The Second Workshop on Financial Narrative Processing of NoDalida 2019}},  
  year      = {2019}  
}

